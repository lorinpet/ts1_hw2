import hw2.Calculator;
import org.junit.jupiter.api.Assertions;

public class CalculatorTest {
    public static void main(String[] args) {
        CalculatorTest calcTest = new CalculatorTest();
        calcTest.additionTest();
        calcTest.subtractionTest();
        calcTest.multiplicationTest();
        calcTest.divisionTest();
    }
    Calculator calc = new Calculator();
    private void additionTest(){
        Assertions.assertEquals(4, calc.add(2, 2));
    }
    private void subtractionTest(){
        Assertions.assertEquals(-25, calc.subtract(17, 42));
    }
    private void multiplicationTest(){
        Assertions.assertEquals(256, calc.multiply(16, 16));
    }
    private void divisionTest(){
        Assertions.assertEquals(32, calc.divide(256, 8));
        Assertions.assertThrows(ArithmeticException.class, ()->{calc.divide(Integer.MAX_VALUE, 0);});
    }
}
